define(function (require) {
  var imageLinks = require('image-links');
  var gifControl = require('gif-control');
  imageLinks.bindImageLinks();
  gifControl.bindImageHeights();
  gifControl.bindStartStop();

});
