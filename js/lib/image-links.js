define(function() {
  return {
    bindImageLinks: function bindImageLinks () {
      $('a[data-image-link]').hover(function(e) {
        $(this).data('old-html', $(this).html());
        $(this).append('<div class="image-link-div"><img src="'+$(this).data('image-link')+'"/></div>');
      }, function(e){
        $('img.image-link-image').remove();
        $(this).html( $(this).data('old-html') );
      });
    }
  };
});
