define(function() {
  return {
    bindImageHeights: function bindImageHeights () {
        function setImageHeights() {
            $('.gif-img').each(function(){
                $el     = $(this);
                $window = $(window);
                var elhgt   = $el.outerHeight();
                var winht   = $window.height();
                // console.log('elhgt', elhgt);
                // console.log('winht', winht);
                var perc = winht / 100;
                var newheight = winht - (perc * 25);

                if(elhgt > winht - (perc*10)){
                    // console.log('newhgt', newheight);
                    $el.height(newheight);
                }
            });
        }

        setImageHeights();
        $(window).resize(setImageHeights);

    },
    bindStartStop: function bindStartStop () {
        $(window).scroll(function(){
            $('.gif-img').each(function(){

                $el     = $(this);
                $window = $(window);

                var elpos  = $el.position()['top']; // fixed
                // var elpos  = this.getBoundingClientRect().top;
                var winpos = $window.scrollTop(); // variale
                var posdif = elpos - winpos;

                var winht   = $window.height();
                var elhgt   = $el.outerHeight();
                var ldist   = (winht - elhgt) / 2; // 140
                var playing = $el.data('playing-anim');

                // console.log('elpos', elpos); // 684
                // console.log('winpos', winpos);
                // console.log('posdif', posdif); // 220
                // console.log('winht', winht);
                // console.log('elhgt ', elhgt);
                // console.log('ldist ', ldist); // 140

                if((posdif < ldist) && playing == '0') {
                    var staticUrl = $el.attr('src');
                    var animUrl   = $el.data('start-frame');

                    $el.attr('src', animUrl);
                    $el.data('start-frame', staticUrl);
                    $el.data('playing-anim', '1');
                }

                if((posdif > ldist) && playing == '1') {
                    var animUrl   = $el.attr('src');
                    var staticUrl = $el.data('start-frame');

                    $el.attr('src', staticUrl);
                    $el.data('start-frame', animUrl);
                    $el.data('playing-anim', '0');
                }

                if(posdif < 0) {
                    var animUrl   = $el.attr('src');
                    var staticUrl = $el.data('start-frame');

                    $el.attr('src', staticUrl);
                    $el.data('start-frame', animUrl);
                    $el.data('playing-anim', '0');
                }

            });
        });
    }
  };
});
