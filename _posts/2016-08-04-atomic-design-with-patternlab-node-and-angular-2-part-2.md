---
layout: post
series: patternlab-angular
title: Atomic Design, Patternlab & Angular 2 - part 2
meta: atomic-design patternlab node angular2
categories:
- frontend
---

{% include series.html series='patternlab-angular' %}

Welcome to part 2 of my multi part series on component development with Angular2 & Patterlab. In the first section we looked at configuring typescript, the next section we will look at setting up Angular 2 rc4.

#### 2. Now we want to install Angular2

**NOTE: The results of this section ( Part 2 ) [are in this commit](https://github.com/stevemartin/patternlab-node-gulp-ng2/commit/1ca1b58d41eac58893d6f4540142efad73d4b7a3)**

{% highlight bash %}
npm install --save @angular/core@2.0.0-rc.4 rxjs@5.0.0-beta.6 'zone.js@^0.6.6'
{% endhighlight bash %}

rxjs and zone.js are dependencies for @angular/core, rxjs is a 'reactive' extension library for building modern reactive apps. zone.js is like thread-local storage for js.


{% highlight bash %}
npm install --save @angular/compiler@2.0.0-rc.4 @angular/common@2.0.0-rc.4 @angular/platform-browser@2.0.0-rc.4 @angular/platform-browser-dynamic@2.0.0-rc.4
{% endhighlight bash %}

I've bundled these npm commands together in order of dependencies.

Now edit `source/ts/circle.ts` and replace it's contents with the following:

{% highlight ts %}
import {Component} from '@angular/core';

@Component({
    selector: '[circle]',
	template: `<svg:circle [attr.fill]="fill" [attr.cx]="cx" [attr.cy]="cy" [attr.r]="r" [attr.stroke]="stroke" [attr.opacity]="opacity" />`
})

export class Circle {
  cx:number = 30;
  cy:number = 30;
  r:number = 29;
  opacity:number = 1.0;
  stroke:string = 'black';
  fill:string = 'darkred';
}
{% endhighlight ts %}

We have replaced the old circle class with a new Angular 2 Component that will render an SVG circle when used. We will come back to this later!

Now, from the command line run:

{% highlight bash %}
gulp pl-typescript
{% endhighlight bash %}

You should see an error:
{% highlight bash %}
source/ts/circle.ts(8,14): error TS1219: Experimental support for decorators is a feature that is subject to change in a future release. Set the 'experimentalDecorators' option to remove this warning.
{% endhighlight bash %}

Now add:
{% highlight json %}
        "experimentalDecorators" : true
{% endhighlight json %}

Under the `"compilerOptions"` key of the `tsconfig.json` file.

Run the task again:
{% highlight bash %}
gulp pl-typescript
{% endhighlight bash %}

Now you should see the transpiled output in `source/js/circle.js`, something like:

{% highlight js %}
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var Circle = (function () {
    function Circle() {
        this.cx = 30;
        this.cy = 30;
        this.r = 29;
        this.opacity = 1.0;
        this.stroke = 'black';
        this.fill = 'darkred';
    }
    Circle = __decorate([
        core_1.Component({
            selector: '[circle]',
            template: "<svg:circle [attr.fill]=\"fill\" [attr.cx]=\"cx\" [attr.cy]=\"cy\" [attr.r]=\"r\" [attr.stroke]=\"stroke\" [attr.opacity]=\"opacity\" />"
        })
    ], Circle);
    return Circle;
}());
exports.Circle = Circle;
{% endhighlight js %}

Excellent, now we have typescript compiling Angular2 code, but we're not done yet. See the `var core_1 = require('@angular/core');` line of code in the above? If we were to execute this in a browser in its current form it would error out on this line, complaining that it doesn't know how to resolve the require or something similar. This takes us on to the third part of this series, dealing with module resolution.

#### 3. System Js

System Js is a dependency resolution tool, and shares some similarity to webpack, here I'm installing the `systemjs-builder` lib which enables it's use in `gulp'.

`npm install --save-dev systemjs systemjs-builder`

6. Install all the dependencies

    `npm install'
