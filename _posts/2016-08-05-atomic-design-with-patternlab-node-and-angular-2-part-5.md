---
layout: post
series: patternlab-angular
title: Atomic Design, Patternlab & Angular 2 - part 5
meta: atomic-design patternlab node angular2
categories:
- frontend
---

{% include series.html series='patternlab-angular' %}

And so to the final part this series on component development with Angular2 & Patterlab. In the last section we looked at bootstrapping an Angular Component. In this section we'll look at bootstrapping multiple components.

#### 4. Working with multiple components

**NOTE: The results of this section ( Part 5 ) [are in this commit](https://github.com/stevemartin/patternlab-node-gulp-ng2/commit/547cdb96ced9cb551b6d0bb46daca21883ad8771)**

This is the last part of the series on using Angular 2 with Patternlab and definitely the most experiemental part.

So far we have one component bootstrapped into the view, but obviously we will want to have multiple components ( it's atomic design after all, right? ).

#### A single bundle
There are a couple of options that I've tried but neither are particularly appealing. The first was to create a `main.ts` file which bootstraps each component, the benefit of using this approach is that it keeps the payload to a minimum, but the problem is that when you navigate into an individual component, you start getting errors from Angular complaining that it can't find the missing selectors for the components that are no longer there.

#### Individual wrappers
The second option is to create individual wrapper 'app' files `circle-app.ts` etc, and have SystemJS bundle everything directly into each one, then add a reference to each bundle in each individual pattern template file. While this stops Angular & the browser console from playing up, it also exponentially increases the payload when you want to load a global view, as there will be a bundled version with every pattern that is loaded. It also means you have to write a lot of boilerplate every time you want to add a new component.

#### Thinking ahead
I think this could probably be solved with some advanced clientside SystemJS programming, which I will get to and hopefully update this article soon.

For now you can [take a look at the diff on github](https://github.com/stevemartin/patternlab-node-gulp-ng2/commit/547cdb96ced9cb551b6d0bb46daca21883ad8771){:target="_blank"} to see where I'm at.


