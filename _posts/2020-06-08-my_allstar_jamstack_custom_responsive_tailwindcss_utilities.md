---
title: My Allstar Jamstack. Custom Responsive TailwindCSS Utilities
layout: post
date: 2020-06-08 08:00 EDT

---

This stack uses Webpack with the default configuration file `webpack.config.js` and a `purgecss.config.js` file. For best and quickest contextual understanding, [check out the source](https://gitlab.com/tevio/allstar-jam/-/tree/v2.2)

We want to be able to add custom utilities to Tailwind so we need to introduce a `tailwind.config.js` file that gets picked up by the build process.

`touch tailwind.config.js` at the root of the project and populate it as per the following diff:-

``` diff
new file mode 100644
index 0000000..e03a1ff
--- /dev/null
+++ b/tailwind.config.js
@@ -0,0 +1,13 @@
+module.exports = {
+  theme: {
+    extend: {
+      inset: {
+        '75per': '50%',
+        '50per': '50%',
+        '25per': '25%'
+      }
+    }
+  },
+  variants: {},
+  plugins: []
+}
```

This change extends TailwindCSS' built in `inset` utility with some additional settings. Now we can reference this in our project CSS like so:-

``` diff
--- a/src/main.css
+++ b/src/main.css
@@ -9,3 +9,53 @@
 body {
   color: white;
 }
+
+@responsive {
+  .top-1\/2 {
+    @apply top-50per;
+  }
+
+  .bottom-1\/2 {
+    @apply bottom-50per;
+  }
+
+  .left-1\/2 {
+    @apply left-50per;
+  }
+
+  .right-1\/2 {
+    @apply right-50per;
+  }
+
+  .top-1\/4 {
+    @apply top-25per;
+  }
+
+  .bottom-1\/4 {
+    @apply bottom-25per;
+  }
+
+  .left-1\/4 {
+    @apply left-25per;
+  }
+
+  .right-1\/4 {
+    @apply right-25per;
+  }
+
+  .top-3\/4 {
+    @apply top-75per;
+  }
+
+  .bottom-3\/4 {
+    @apply bottom-75per;
+  }
+
+  .left-3\/4 {
+    @apply left-75per;
+  }
+
+  .right-3\/4 {
+    @apply right-75per;
+  }
+}
```

And now we can use these new inset settings in our HTML like this:-

``` diff
--- a/index.markdown
+++ b/index.markdown
@@ -9,8 +9,8 @@ layout: home
   <h1>Home</h1>
   <a href='/transition'>Transition</a>

-  <div data-controller="hello">
-    <input data-target="hello.name" type="text">
+  <div class='absolute top-1/4 left-1/4 md:top-1/2 w-1/2' data-controller="hello">
+    <input class='text-black' data-target="hello.name" type="text">
     <button data-action="click->hello#greet">Greet</button>
   </div>
 </div>
```

The source code for this tutorial is available [here](https://gitlab.com/tevio/allstar-jam/-/tree/v2.2)
