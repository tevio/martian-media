---
title: Blockchain Efficiency vs Traditional Block Efficiency
layout: post
date: 2017-11-06 17:13 GMT

meta: bitcoin blockchain
categories:
- bitcoin
---

I was recently informed by my esteemed compadre about the energy intensity of [Bitcoin transaction Mining](https://motherboard.vice.com/en_us/article/ywbbpm/bitcoin-mining-electricity-consumption-ethereum-energy-climate-change){:target='_blank'}. Currently a single transaction takes the same amount of power as it takes to power three or four U.S homes for a single day. While this is substantial, the nice thing about blockchain mining is that it is actually quantifieable.

Conversly, how do you quantify the total energy consumption required to build and maintain this:-

![London](/images/canada-water.jpg)

#### And this:-

![Pudong](/images/Shanghai_pudong.jpg)

#### And this:-

![New York](/images/new-york-financial.JPG)

<br>
<br>
#### All of which need to be operated daily by people living in these:-

<br>
<br>

![Suburbia](/images/suburbia.jpg)

#### And travelling using this ..

![Highways](/images/m25.jpe)

#### And these ..

![Airports](/images/heathrow.jpe)

<br>
<br>

I must admit I'm having some trouble seeing how the computing power required to validate Bitcoin transactions in the network comes anywhere near what's required to power the current industrial complex it will hopefully replace, now or in the even distant future??

Difficult to quantify no? Is there any clear metric for the amount of energy required to validate each transaction in the traditional way?
<br>
<br>
<br>
<br>
