---
layout: post
series: patternlab-angular
title: Atomic Design with Patternlab & Angular 2 - part 1
meta: atomic-design patternlab node angular2
categories:
- frontend
---


{% include series.html series='patternlab-angular' %}

<br>
<strong>Welcome to part one of my multi part series on component development with Angular 2 & Patternlab Node.</strong> With web components becoming all the rage and component based frontend frameworks taking up the reigns of frontend development. I thought I'd take a look at mixing some great open source tools that will enable you to build better components more collaboratively than ever.

If you just want see/run the code for this series, you can clone it from my [Github Repo](https://github.com/stevemartin/patternlab-node-gulp-ng2){:target="_blank"}
<br>
<br>

<div class='flex-cards'>
  <div class='row'>
    <div class='col-sm-6 col-md-6'>
      <div class='technology-card panel panel-default'>
        <div class='technology-card--head'>
          <h4>Patternlab-Node</h4>
        </div>
        <div class='technology-card--icon'>
          <img src="/images/icon-atom.svg" />
        </div>
        <div class='technology-card--content'>
        <a href='http://patternlab.io/'>Patternlab</a> is a tool for building component libraries ( or styleguides ) for the frontend, it encourages a high degree of heirarchal separation between design elements aka atomic design. It's available in both PHP and node versions.
        </div>
      </div>
    </div>
    <div class='col-sm-6 col-md-6'>
      <div class='technology-card panel panel-default'>
        <div class='technology-card--head'>
          <h4>AngularJS 2</h4>
        </div>
        <div class='technology-card--icon'>
          <img src="/images/AngularJS_logo.svg" />
        </div>
        <div class='technology-card--content'>
          The long awaited <a href='https://angular.io'>Angular 2</a> now has release candidates in the wild, woop! We'll be using the <code>rc.4</code> version for the rest of this series.
        </div>
      </div>
    </div>
  </div>
</div>

> `CAUTION!` Angular 2 is still beta software so caution is advised, it's implementation could change at any moment! Please stay locked to `rc.4` for this series.

#### Prerequisites

* Nodejs
* Git
* Unix command line
* You'll need the Nodejs edition of Patternlab, which you can [download here](https://github.com/pattern-lab/edition-node-gulp/releases){:target="_blank"}.

### 1. Set up Typescript.

**NOTE: The results of this section ( Part 1 ) [are in this commit](https://github.com/stevemartin/patternlab-node-gulp-ng2/commit/7afbff548136cadd56bbbe92b9425e286575be3f)**

From a terminal cd into the `edition-node-gulp` folder you just downloaded.

Now edit the `.gitignore` file and add a line with `typings/`, if you plan on commiting this to git this is advisable.

Angular2 leverages Typescript for a multitude of reasons ( which I'm not going to go into in this post ), to get it set up issue the following command lines:

{% highlight bash %}

npm install -g --save-dev typescript

tsc --init

npm install --save-dev gulp-typescript typings

npm install

typings init

typings install --source dt --global core-js --save

typings install es6-promise --save

mkdir source/ts

{% endhighlight bash %}

Now edit the `packages.json` file and add the following just before the last parenthesis:

{% highlight json %}
"scripts": {
  "postinstall": "typings install"
}
{% endhighlight json %}

In the `patternlab-config.json` file at the root of the project add `"ts" : "./source/ts"` after the `"js"` key, under the `"source"` key. the file should now look like (excluding ellipses):

{% highlight json %}
    "source" : {
      ...
      "js" : "./source/js",
      "ts" : "./source/ts",
      ...
    }
{% endhighlight json %}

Now, edit the `gulpfile.js` at the root of the project: We need to require some files:

{% highlight js %}
...
ts = require('gulp-typescript'),
tsProject = ts.createProject("tsconfig.json"),
...
{% endhighlight js %}

And ( in the same file ) add a gulp task for transpiling typescript ...

{% highlight js %}
/******************************************************
 * TRANSPILE TYPESCRIPT - stream assets from source to destination
******************************************************/
gulp.task('pl-typescript', function(){
  return tsProject.src('**/*.ts', path.resolve(paths().source.ts))
    .pipe(ts(tsProject))
    .js.pipe(gulp.dest(path.resolve(paths().source.js)))
})
{% endhighlight js %}

This sets up typescript as we need it.

Now we want to test transpiling some typescript.

{% highlight bash %}

echo "export class Circle {}" > source/ts/circle.ts
gulp pl-typescript

{% endhighlight bash %}

And you should now have a transpiled file js at: `source/js/circle.js`, the contents of which should look a bit like:

{% highlight js %}
"use strict";
var Circle = (function () {
    function Circle() {
    }
    return Circle;
}());
exports.Circle = Circle;
{% endhighlight js %}

Congratulations! You can haz Typescript and Patternlab! Tune in for part two to follow shortly!

