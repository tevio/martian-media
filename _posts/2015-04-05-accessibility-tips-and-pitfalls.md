---
date: 2015-04-05
layout: post
title: Accessibility 2015
meta: web accessibility a11y
categories:
- a11y
- web
---

# Accessibility
I recently did a short contract focussed around web accessibility for a client. It's an interesting field that comes under the banner of responsive design that has it's own set of compatability issues thanks to the many different devices and proprietary platforms that vendors provide for reading web sites accessibly.


## Resources
There are a multitude of different tools and several standard's for web accessibility.

### Web Sites & Blogs

#### [The Roles Model](http://www.w3.org/TR/wai-aria/roles){:target="_blank"}
The aria roles taxonomy.

#### [Accessible Culture](http://accessibleculture.org/){:target="_blank"}
This is a great resource for accessibility info. [Here](http://accessibleculture.org/research-files/WDCNZ-25July2013/make-your-widgets-sing-with-aria.html#/18){:target="_blank"} is a useful infographic.

#### Accessibility APIs
[Why AAPI's Matter](https://www.marcozehe.de/2013/09/07/why-accessibility-apis-matter/)
[W3C Guidelines](http://www.w3.org/TR/html-aapi/){:target="_blank"}

### Screenreaders
[Chromevox](http://www.chromevox.com/){:target="_blank"}
[Google Talkback](https://play.google.com/store/apps/details?id=com.google.android.marvin.talkback&hl=en_GB){:target="_blank"}
[JAWS](http://www.freedomscientific.com/Downloads/JAWS){:target="_blank"}
[VoiceOver (Mac)](https://www.apple.com/accessibility/osx/voiceover/){:target="_blank"}
[NVDA](http://www.nvaccess.org/){:target="_blank"}

### Validation & Checking Tools

#### Google Chrome - Accessibility Developer Tools
Picks up CSS
Doesn't pick up table scopes

#### MauveWeb
Seems to have good coverage. I found this tool to be quite useful for checking aria.

[MauvWeb](http://hiis.isti.cnr.it:8080/MauveWeb){:target="_blank"}

[MauvWebValidate](http://hiis.isti.cnr.it:8080/MauveWeb/Validate){:target="_blank"}
td's with no scope don't get flagged

#### AChecker

[AChecker](http://achecker.ca/checker/index.php){:target="_blank"}

Table Scopes
Support  | No Support
---------------------
MauveWeb |
         | Chrome Accessibility Developer Tools
         | AChecker

#### Access Lint
[Access Lint](https://github.com/accesslint/access_lint){:target="_blank"}
Ruby Gem

### Accessibility Librarys

### Platform testing
Android Studio - Google Talkback
Android Emulator - install Google Play Servicces
modern.ie

