---
layout: post
title: Today I release Worklist to the world!
meta:
categories:
- oss
- web
- cv
---

<div class='row'>
<div class='col-md-6 col-sm-6 col-lg-6'>
<h4>Edit your CV online</h4>
<img src="https://raw.githubusercontent.com/stevemartin/worklist/master/wl-edit-mode.png">
</div>
<div class='col-md-6 col-sm-6 col-lg-6'>
<h4>Then view it in HTML</h4>
<img src="https://github.com/stevemartin/worklist/raw/master/wl-view-mode.png">
</div>
</div>

<hr>
<a target='_blank' href='https://github.com/stevemartin/worklist/tree/v1.0.0-alpha' class='btn btn-primary' role='button'>View on Github</a>
<hr>

#### Open source app

It's not super spectacular, but me & a buddy worked on this app on and off in our spare time in an effort to build a Rails/Angular hybrid for the learning experience and the vague notion of taking over the world of CV generation which, sadly, never happened because we were too busy with other stuff.

The idea was to have a CV template that can be created and edited inline in the browser, with no initial sign up ( woop! sessions. ), that closely resembles the rendered HTML view and which you can also download as a PDF to share ( the pdf generation is still a bit experiemental and doesn't work great on heroku yet ).

This app combines an Angular 1 frontend with a Rails 4 API & backend in a single repo, so for anyone interested in seeing how that works, please take a look at the source, clone it, run it, knock yourself out. That said, in its current form it's not using the bleeding edge versions of either. The code is not the nicest, the UI is not the cleanest, but it has full integration tests so it definitely works! If you feel like improving the codebase or proposing a feature, please feel free to fork & submit a PR.

If you want to host and maintain your own CV online, [you can deploy this app to heroku in a few easy steps](https://github.com/stevemartin/worklist#end-to-end-deployment){:target="_blank"} and take it from there. If you want to customise the theme or components, please feel free to fork it.

The app is pretty much still an MVP, we had a load of ideas for new features but of course time is limited. Some of the feature ideas that are not yet implemented are:

* Invite only viewing - this would enable you to lock the CV and send single view links to individual emails.
* User CSS editing ( Remember MySpace! ) - upload your own custom theme for your CV
* Drag and drop components to enable you to customise your CV further.
* CV Blockchain ( linking all the CV's that want to be searchable in a distributed network ).
* Theme library so that users can pick from predefined CV themes.

Hope you like it!

