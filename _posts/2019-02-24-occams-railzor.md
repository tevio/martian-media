---
title: Occams Railzor
layout: post
date: 2019-02-24 19:58 GMT

---

I want to get a form application up and running to collect specific data and print a ticket, the requirements are:

1. It has a single entry form, where it collects a user details and ticket details
2. It stores the ticket data in a datastore
3. It needs to have tests
4. It validates the format of the email
5. It generates a printable ticket

Optional dependencies
1. It needs to work as an API
2. It needs to have authentication
3. It needs to have a responsive UI
4. It needs an avatar
5. It needs to be shareable on social media

To do this in Rails:-

``` bash
rails new -B tickets
cd tickets
bundle --path .bundle
bundle add rspec-rails --group "development, test"
bundle exec rails generate rspec:install

bundle exec rails g scaffold ticket event_name:string starts:datetime recipient_email:string "ticket_price:decimal{5,2}" recipient_name:string recipient_code:string recipient_postcode:string venue:string venue_postcode:string

bundle exec rake db:create db:migrate
bundle exec rails s
```

Change `app/models/ticket.rb` to read

``` ruby
class Ticket < ApplicationRecord
  validates_format_of :recipient_email, :with => /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/
end
```

Change `app/views/tickets/show.html.erb` to read:

``` erb
<p id="notice"><%= notice %></p>
<section id='ticket'>
  <div id='event-details'>

    <p>
    <h1><%= @ticket.event_name %> </h1>
    </p>

    <p>
    <h2><%= @ticket.starts.to_s(:db) %> </h2>
    </p>

    <p>
    <h2><%= @ticket.venue %> </h2>
    </p>
  </div>

  <div id='ticket-recipient-details'>
    <p>
    <strong>Ticket Price:</strong>
    <%= @ticket.ticket_price %>
    </p>

    <p>
    <strong>Recipeint Name:</strong>
    <%= @ticket.recipient_name %>
    </p>

    <p>
    <strong>Recipient code:</strong>
    <%= @ticket.recipient_code %>
    </p>

  </div>
</section>

<section id="web-links">
<%= link_to 'Edit', edit_ticket_path(@ticket) %> |
<%= link_to 'Back', tickets_path %>
</section>
```

And `app/assets/stylesheets/tickets.scss` to read:

``` scss

@media print, screen {
#ticket {
  display: flex;
  flex-direction: row;
  border: solid 1px black;
  padding: 1em;
  align-items: center;
  justify-content: center;
  width: 550px;

  h1 { font-size: 2.5em }

  #event-details {
    flex-grow: 1;
    border-right: dotted 1px grey;
    padding: 1em;
  }

  #ticket-recipient-details {
    flex-grow: 1;
    padding: 1em;
  }
}
}

@media print {
  #web-links {
    display: none;
  }
}
```

That covers off **Steps 1 to 5** and gives you:

* a database
* a database schema
* the code
* the forms
* the tests
* the html templates
* the web server
* the test runner
* A web view and a printable view of the ticket.

It's RESTful and can be adapted to serve JSON in a very simple set of steps ( mostly just add `.json` to the url)

Can anybody show me the steps required to obtain the same functionality, using one of the popular frontend frameworks, as a SPA for example, for the templating side of this?
