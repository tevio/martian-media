---
layout: post
title: Theming Components with SCSS Mixins
meta: React Scss Composition Theming
categories:
- react
- sass
- theming
---

<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/426420171&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>

In a recent blog post about [React style composition with SCSS](/js/react/sass/css/2017/10/21/using-sass-mixins-with-react.html){:target="_blank"} I was asked about theming in relation to composable SCSS mixins. The good thing is, this approach works perfectly fine with theming, because SCSS supports variables and named parameters, as demonstrated in the code below:-

You could declare the base theme default file like so:-
{% highlight scss %}
// defaultVariables.scss

$defaultBackgroundColor: blue;
$defaultColor: red;
{% endhighlight scss %}

Then, you can declare parameterized mixins which import the theme defaults `defaultVariables.scss` and which leverage named arguments. These named arguments can be overiden in different contexts to enable composition, but also fall back to the current theme by default if nothing is specified.
{% highlight scss %}
// spinnerComp.scss

@import './defaultVariables.scss'

// now pass in the defaults from defaultVariables.scss in the mixin declaration like this
@mixin spinnerComp($backgroundColor: $defaultBackgroundColor, $color: $defaultColor) {
  &--spinner {
    background-color: $backgroundColor;
    color: $color;
  }
}

{% endhighlight scss %}

Then, include the mixin in different contexts and skin accordingly.
{% highlight scss %}
// arbitrary contexts

.page {
  @include spinnerComp();
}

.list-item {
  // Note that you can now provide named parameters to the mixin instance, meaning that you can keep some, all or none of the original defaults as you see fit.
  @include spinnerComp($color: green, $backgroundColor: yellow);
}

.card {
  @include spinnerComp($color: orange);
}
{% endhighlight scss %}

As you can see, if you change the variables in `defaultVariables.scss` then any conventional instances of spinnerComp will inherit those new variables, enabling nice heirarchal theming.

#### Here is a [jsFiddle](https://jsfiddle.net/tevio/334j8t8v/){:target="_blank"} to demonstrate the technique.

