---
layout: post
title: Discover MAC addresses on your local network with OSX.
meta: network security MAC osx commandline
categories:
- osx
- network
- security
- commandline
---
<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/210778852&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>

Sometimes you'd like to see what kind of devices are currently on your local network, one way this could be achieved is by discovering and then interrogating the MAC addresses of those devices. You can do this quite easily with a few terminal pipes.

## The Short
Run these two commands in a terminal:-

{% highlight bash %}
brew install jq
arp -a | grep -v incomplete | cut -d ' ' -f 4 | xargs -I % curl http://www.macvendorlookup.com/api/v2/% | jq
{% endhighlight bash %}

## The Long
`arp` is a tool for checking addresses in the _Address Resolution Table_.

`arp -a` will give you a list of all the addresses, possibly something like:

{% highlight bash %}
? (192.168.0.1) at ad:46:1c:af:2c:dc on en0 ifscope [ethernet]
? (192.168.0.2) at (incomplete) on en0 ifscope [ethernet]
? (192.168.0.14) at df:37:6b:2b:11:fc on en0 ifscope [ethernet]
? (192.168.0.20) at 74:8f:2c:ba:14:2e on en0 ifscope [ethernet]
? (192.168.0.21) at (incomplete) on en0 ifscope [ethernet]
? (192.168.0.22) at 2a:e1:ef:76:a0:29 on en0 ifscope [ethernet]
? (192.168.0.24) at 7d:a2:a5:09:27:b2 on en0 ifscope [ethernet]
? (192.168.0.25) at (incomplete) on en0 ifscope [ethernet]
? (192.168.0.26) at bc:66:0f:03:7d:cd on en0 ifscope [ethernet]
? (192.168.0.255) at (incomplete) on en0 ifscope [ethernet]
{% endhighlight bash %}

Some of these, as you can see, will be __incomplete__, so we can filter them out using `grep` like so:-

`arp -a | grep -v incomplete`

OK! Now we have a list with some MAC addresses that we can play with, but they're not very useful yet. We can fix that with the age old unix facility `cut`:-

`arp -a | grep -v incomplete | cut -d ' ' -f 4`

And we get...

{% highlight bash %}
ad:46:1c:af:2c:dc
6c:ad:f8:9d:db:33
df:37:6b:2b:11:fc
74:8f:2c:ba:14:2e
7d:a2:a5:09:27:b2
{% endhighlight bash %}

... or similar.

Great now we have a nice clean list of MAC addresses that we can do interesting things with, like finding out what some of them *actually are*, using a nice little web service I found.

`arp -a | grep -v incomplete | cut -d ' ' -f 4 | xargs -I % curl http://www.macvendorlookup.com/api/v2/%`

Now we have some data about our MAC addresses, but alas, it is not pretty.

{% highlight bash %}
[{"startHex":"4C60DE000000","endHex":"4C60DEFFFFFF","startDec":"84528925113451","endDec":"87778941890589","company":"NETGEAR","addressL1":"350 East Plumeria Drive","addressL2":"","addressL3":"San Jose California 95134","country":"UNITED STATES","type":"MA-L"}][{"startHex":"6CADF8000000","endHex":"6CADF8FFFFFF","startDec":"151494445891617","endDec":"449494462668825","company":"Azurewave Technologies, Inc.","addressL1":"8F., No.120, Sec. 2, Kongdao 5 Rd., Hsinchu, Taiwan 30056","addressL2":"","addressL3":"Taipei Taiwan 231","country":"TAIWAN, PROVINCE OF CHINA","type":"MA-L"}][{"startHex":"58946B000000","endHex":"58946BFFFFFF","startDec":"97394473566373","endDec":"97474490343494","company":"Intel Corporate","addressL1":"Lot 8, Jalan Hi-Tech 2\/3","addressL2":"Kulim Hi-Tech Park","addressL3":"Kulim Kedah 09000","country":"MALAYSIA","type":"MA-L"}]%
{% endhighlight bash %}

Never fear, a cool utility is here!

`brew install jq`

This is the ( now defacto ) command line JSON parsing tool to Make All Things Pretty.

Now we can pipe the output of our `curl` into `jq`

`arp -a | grep -v incomplete | cut -d ' ' -f 4 | xargs -I % curl http://www.macvendorlookup.com/api/v2/% | jq`

