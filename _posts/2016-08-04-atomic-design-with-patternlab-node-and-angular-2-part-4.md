---
layout: post
series: patternlab-angular
title: Atomic Design, Patternlab & Angular 2 - part 4
meta: atomic-design patternlab node angular2
categories:
- frontend
---

{% include series.html series='patternlab-angular' %}

Welcome to part 4 of my multi part series on component development with Angular2 & Patterlab. In the last section we looked at configuring SystemJS and bundling our code. In this section we'll look at bootstrapping our component into Patternlab.

#### 4. Bootstrapping with dependencies

**NOTE: The results of this section ( Part 4 ) [are in this commit](https://github.com/stevemartin/patternlab-node-gulp-ng2/commit/28ac2cb0f770584b48b5e685f8490cd0eb814995)**

First, issue the following:
{% highlight bash %}
npm install core-js reflect-metadata --save
{% endhighlight bash %}

Then copy the relevant files:-
{% highlight bash %}
cp node_modules/reflect-metadata/Reflect.js source/js
cp node_modules/zone.js/dist/zone.min.js source/js
cp node_modules/core-js/client/shim.min.js source/js
{% endhighlight bash %}

Now add the following lines to `source/_meta/_00-head.mustache`

{% highlight html %}
    <script src="../../js/shim.min.js"></script>
    <script src="../../js/zone.js"></script>
    <script src="../../js/Reflect.js"></script>
{% endhighlight html %}

There's probably a more elegant way to do this, which I am open to suggestion on.

Now edit `source/ts/circle.ts' so it reads like so:

{% highlight ts %}
import {Component} from '@angular/core';
import { bootstrap } from '@angular/platform-browser-dynamic';

@Component({
    selector: '[circle]',
	template: `<svg:circle [attr.fill]="fill" [attr.cx]="cx" [attr.cy]="cy" [attr.r]="r" [attr.stroke]="stroke" [attr.opacity]="opacity" />`
})

export class Circle {
  cx:number = 30;
  cy:number = 30;
  r:number = 29;
  opacity:number = 1.0;
  stroke:string = 'black';
  fill:string = 'darkred';
}

bootstrap(Circle)

{% endhighlight ts %}

Now edit the `gulpfile.js` so that the `pl-systemjs` tasks `packages` section *also* has the following packages:-

{% highlight js %}
...
'@angular/common' : { main: 'index.js' },
'@angular/compiler' : { main: 'index.js' },
'@angular/platform-browser' : { main: 'index.js' },
'@angular/platform-browser-dynamic' : { main: 'index.js' },
...
{% endhighlight js %}

Additionally, modify the existing tasks `pl-assets` and add our new tasks so it reads:

{% highlight js %}
gulp.task('pl-assets', gulp.series(
  'pl-typescript',
  'pl-systemjs',
  gulp.parallel(
...
{% endhighlight js %}

Note that I added our tasks *before* the parallel section.

Also modify the `function watch()` function so it watches for typescript changes:-

{% highlight js %}
function watch() {
  gulp.watch(path.resolve(paths().source.ts, '**/*.ts')).on('change', gulp.series('pl-typescript', 'pl-systemjs','pl-copy:js', reload));
...
{% endhighlight js %}

Now add the following lines to `source/_meta/_01-foot.mustache`:

{% highlight html %}
  <!-- render after content due to this - http://stackoverflow.com/questions/37944263/jspm-angular2-rc2-build-animate-error -->
  <script src="../../js/circle.sfx.js"></script>
{% endhighlight html %}

#### And that's it, you should now have fully functional Angular with Patternlab!
