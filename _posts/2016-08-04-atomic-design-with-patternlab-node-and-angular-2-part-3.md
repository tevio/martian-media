---
layout: post
series: patternlab-angular
title: Atomic Design, Patternlab & Angular 2 - part 3
meta: atomic-design patternlab node angular2
categories:
- frontend
---

{% include series.html series='patternlab-angular' %}

Welcome to part 3 of my multi part series on component development with Angular2 & Patterlab. In the last section we looked at configuring Angular2 and transpiling our first component with Typescript. In this section we'll look at server side module resolution and bundling for use.

#### 3. SystemJS

**NOTE: The results of this section ( Part 3 ) [are in this commit](https://github.com/stevemartin/patternlab-node-gulp-ng2/commit/f28974f1e5fdd5c77f15b9a7123fd478b631986e)**

SystemJS is a dependency resolution tool, and shares some similarity to webpack, here I'm installing the `systemjs-builder` lib which enables it's use in `gulp`.

First, issue the following:
{% highlight bash %}
npm install --save-dev systemjs systemjs-builder
{% endhighlight bash %}

Now add the following task to `gulpfile.js`

{% highlight js %}
/******************************************************
 * BUILD JAVASCRIPT - resolve modules and bundle as static
******************************************************/
gulp.task('pl-systemjs', function(){
    var builder = new Builder;
    builder.config({
        map: {
            '@angular': 'node_modules/@angular',
            'rxjs': 'node_modules/rxjs'
        },
        packages: {
            '@angular/core' : { main: 'index.js' },
            'rxjs': { defaultExtension: 'js' }
        }
    })

    return builder.buildStatic('source/js/circle.js', 'source/js/circle.sfx.js', {
        globalName: 'plCircle'
    })
})
{% endhighlight js %}

The `map` section in `builder.config` tells SystemJS where to find things referenced in `require` statments and the `packages` section explicitly grants access to packages that are required and nothing else.

Now run the task from the command line:
{% highlight bash %}
gulp pl-systemjs
{% endhighlight bash %}

This will output a file `source/js/circle.sfx.js`. Be warned, this file will be huge, because it includes all the dependencies the Angular 2 Circle component needs to run in the broweser without ( most ) other dependencies.

You now have a working bundle!
