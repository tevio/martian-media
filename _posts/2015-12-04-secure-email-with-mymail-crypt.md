---
layout: post
title: Secure Gmail with Mymail-Crypt and Chrome
meta: gmail chrome security extensions gpg mymail-crypt
categories:
- chrome
- gmail
- pgp
- security
- mymail-crypt
---

I recently made this guide for a friend, she seemed pretty happy with it so I'll share it. Ok, so there is a little bit of set up, but once it's done you won't need to do it again.

Please note I'm not the creator of this technology, just found it useful and want to share, also there is an introductory video on the chrome store page for this plugin but it's quite old and not very informative as to how you *actually* use it! :D

In light of recent high profile hacks and given that email is a highly insecure medium *anyway* I thought I'd turn this thread into an animated gif tutorial for non technical people.

So you have a Gmail acount and you have Chrome right? Well guess what, you're in luck. So you want to send a sensitive message to someone for *whatever reason*. Did you know that email is *highly insecure* without encryption? And historically, setting up the tools to encrypt and decrypt email was an order of magnitude more complicated than most people can handle? Well, no longer ....

<div class='anim-step'>
  <div class='step-instructions'>
  <h3>Step 1</h3>
  First, install the mymail-crypt extension from the chrome store  <a href='https://chrome.google.com/webstore/detail/mymail-crypt-for-gmail/jcaobjhdnlpmopmjhijplpjhlplfkhba'>Here</a>.
  </div>
  <div class='gif'><img data-playing-anim='0' src='/images/gifs/get-my-mail-crypt.png' class='gif-img' data-start-frame="/images/gifs/get-my-mail-crypt.png" alt="Open Chrome Extensions" title="Open Chrome Extensions"></div>
</div>
<div class='anim-step'>
  <div class='step-instructions'>
  <h3>Step 2</h3>
  Now go to Chrome's settings and navigate to Extensions
  </div>
  <div class='gif'><img data-playing-anim='0' src='/images/gifs/open-chrome-extensions/out-static-001.png' class='gif-img' data-start-frame="/images/gifs/open-chrome-extensions/animation.gif" alt="Open Chrome Extensions" title="Open Chrome Extensions"></div>
</div>

<div class='anim-step'>
  <div class='step-instructions'>
  <h3>Step 3</h3>
  Find Mymail-Crypt in the list of installed extension and click on 'options'
  </div>
  <div class='gif'><img data-playing-anim='0' src='/images/gifs/mymail-crypt-options/image-00001.jpg' class='gif-img' data-start-frame="/images/gifs/mymail-crypt-options/animate.gif" alt="Open Mymail-Crypt Extension" title="Open Mymail-Crypt Extension"></div>
</div>

<div class='anim-step'>
  <div class='step-instructions'>
  <h3>Step 4</h3>
  <p>
    Then click 'my keys' and then 'generate a new key' and fill in the details accordingly - adding a password will just make it that little bit more secure.
  </p>

  <p>
     NOTE: Sometimes it doesn't work the first time - when you press 'submit' it takes you back to 'home' and you have to do this step again - the second time WILL work though! Open source software - buggy, but great!
  </p>

  </div>
  <div class='gif'><img data-playing-anim='0' src='/images/gifs/generate-public-private-keypair/out-static-010.png' class='gif-img' data-start-frame="/images/gifs/generate-public-private-keypair/animation.gif" alt="Generate Public/Private Keypair" title="Generate Public/Private Keypair"></div>
</div>


<div class='anim-step'>
  <div class='step-instructions'>
  <h3>Step 5</h3>
  <p>
    Go to 'friends keys', now you should see your public key listed there, click 'show key', like so:
  </p>
  <p>
    Now paste it into an email and send it to whoever you want to conduct secure email with. ( thats why its called a public key - it's the counterpart to your private key - which shouldn't be shared )
  </p>
  </div>
  <div class='gif'><img data-playing-anim='0' src='/images/gifs/paste-public-key-to-email/out-static-043.png' class='gif-img' data-start-frame="/images/gifs/paste-public-key-to-email/animation.gif" alt="Show Public Key" title="Show Public Key"></div>
</div>
