const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require("path");
const glob = require("glob-all");
const PurgecssPlugin = require("purgecss-webpack-plugin");

module.exports = {
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader", {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              sourceMap: true,
              plugins: [
                require('tailwindcss'),
                require('autoprefixer'),
              ],
            },
          }]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin(),
    new PurgecssPlugin({
      paths: glob.sync([
        path.join(__dirname, "**/*.html"),
        path.join(__dirname, "dist/**/*.js")
      ]),
      extractors: [
        {
          extractor: content => content.match(/[A-z0-9-:\/]+/g) || [],
          extensions: ["html", "js"]
        }
      ]
    })
  ]
};
