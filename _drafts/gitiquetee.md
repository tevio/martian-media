---
layout: post
title: Gitiquette
meta: git etiquette
categories:
- git
- etiquette
- revision control
---

This is my take on Gitiquette.

Software developemt equals tradeoffs. Git usage is one of them. Sometimes it's better to structure you commits ( and code )
in a way that's more sociable to other developers. This concept can take a little while to sink in when you're starting out
and indeed it did for me. Sometimes a little bit of structure can make all the difference.

### Small teams vs big teams
When you're working in a small team there is often much less need for Gitiquette. However it is not completely irrelevant.

### Topic branches
A good habit to get into is using topic branches.

### Never force `push` production!
This is possibly the worst thing you could do! Other developers will either see this as very stupid or very questionable.

The reasons for this are:

1) You could be attempting to hide a change in the history ( potentially introducing a vulnerability or covering up a previous bug ).
2) You could be overwriting someone elses changes without realising it.

ALWAYS do a `pull` first and then `push` when the remote can be fast forwarded.

### Format your commit messages
There is a good article on this subject [time pope](here).

### Write meaningful commit messages
Try to think about the next person who will be reading through your changes deown the line.

### Squash your topic branch before merging.
It's the WORST thing when you see someone merge changes into master and they're a long bunch of commits with pointless titles like:

`add the var`
`remove the var`
`add the var again`
`GO AWAY filter`
`meh ... no var assigned`

### Small changes
If you're working on a feature that has lots of complex changes there is a good chance you could break it down and make commits that are less
daunting for another developer to read or understand.

For example:

commit 1: migrations for change
commit 2: add backend code that will use aforementioned migrations
commit 3: add frontend code that will make the feature usable

### Code reveiew
Before blinding merging your changes to master does it really hurt to let another developer take a brief look?

Be discerning about this
