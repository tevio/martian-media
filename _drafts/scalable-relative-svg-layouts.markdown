---
layout: post
title: Fluid, Responsive  Sunburst with SVG.
meta: svg sunburst layouts
categories:
- svg
- frontend
- web
---

## I want interesting layouts!

SVG support is [pretty good nowadays](http://caniuse.com/#feat=svg) and with the advent of mobile web browsing in modern smartphones making a scalable vector that is responsive can be quite useful and fun.

### Getting good angles.

First, you need to set up the parent svg, without it the responsiveness will not work.

* Set width and height to be 100%.
* Set positioning to be absolute.

{% highlight html %}

  <svg style='position: absolute; width: 100%; height: 100%;'
    id='svgContainer' x='0' y='0' width='100%' height='100%' >

{% endhighlight html %}

Next, we need a nested svg with a `viewBox`.

* Set viewBox
* Set preserveAspectRatio to none to avoid having to oversize paths.

{% highlight html %}

    <svg preserveAspectRatio='none' viewBox='0 0 100 100'>

{% endhighlight html %}

### Using relative path dimensions vs absolute.

Consider the following attribute:

{% highlight html %}
d="M 50 50 l 50 0 l 0 15"
{% endhighlight html %}

All of our paths will start with this coordinate:-

{% highlight html %}
  M 50 50
{% endhighlight html %}

This will give us a 'horizon point' for each path that stays in the centre of the screen no matter what.


