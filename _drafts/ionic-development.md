---
layout: post
title: Ionic development
meta: ionic development
categories:
- ionic2
- development
- angular2
---

# Get the bleeding edge ionic

npm install -g ionic@beta

To start a blank ionic 2 project with typescript ( necessary for angular2 )
ionic start CollisionMarsFlow blank --v2 --ts

node-sass
https://github.com/sass/node-sass/issues/1265

brew install gcc

Error: spawn EACCES

ionic hooks add

Error: Failed to find 'ANDROID_HOME' environment variable. Try setting setting it manually.


## Angular dependency injection
Import is seperate from provide, IE, if the root component provides, you only need to import in the child and inject in the constructor

♬ ♫ ♬ ♫  Your Ionic app is ready to go! ♬ ♫ ♬ ♫

Make sure to cd into your new app directory:
  cd CollisionMars

To run your app in the browser (great for initial development):
  ionic serve

To run on iOS:
  ionic run ios

To run on Android:
  ionic run android

To test your app on a device easily, try Ionic View:
  http://view.ionic.io

New! Add push notifications, update your app remotely, and package iOS and Android apps with the Ionic Platform!
https://apps.ionic.io/signup

New to Ionic? Get started here: http://ionicframework.com/docs/v2/getting-started
