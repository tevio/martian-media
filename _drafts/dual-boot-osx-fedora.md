---
layout: post
title: Dual boot OSX and Fedora
meta: osx fedora dual boot linux
categories:
- osx
- fedora
- linux
- dual boot
---

rEFInd is the tool

## EFI & UEFI
Extensible Firmware Interface

EFI is not BIOS - see this article

Is your mac EFI or BIOS. If it's intel, it's EFI.

## rEFInd

Refind supports the following features

* Text and graphical mode, will go into more detail.
* Auto detection of EFI & BIOS *boot loaders*
* Configurable graphics
* Launch EFI boot loaders
* Launch legacy BIOS on Macs
* Launch external EFI shell or disk partitioner ????
* gptsync ( guid partition table ) - only for dual booting windows with BIOS ( old )
* Set OS specific bot options
* Load EFI drivers for filesystems or hardware not supported by your firmware ????
* Inclusion of esoteric filesystem drivers
* The ability to specify a configuration file !!! IMPORTANT
* User configurable methods of detecting bootloaders !!! IMPORTANT
  * auto detection of EFI bootloaders
  * auto detection of legacy BIOS bootloaders
  * Manually via configuration file !!! COULD BE HANDY


Interestingly rEFInd purports to have less features for Macs than its predicessor rEFIt.

## OSX El Capitan's SIP & rEFInd

OSX 10.11 includes a new feaure know as SIP aka 'rootless' mode.

## About `bless`

Bless is OSX's 'volume bootability and startup disk options' tool. It allows you to 'bless' a directory which causes the system firmware to look in that directory for boot code.

It supports six modes
* Folder mode - allows you to select a folder on a mounted volume to act as the blessed directory.
* Mount mode is not permanent, it points the system firmware to s specific volume, while assuming the volume has already been blessed ( in Folder Mode ).
* Device Mode is like Mount Mode but allows you to select an unmounted volume
* Netboot Mode - guess
* Info Mode prints out currently blessed directory of a volume BE AWARE THIS IS KNOWN TO CORRUPT THINGS, DONT USE IT.
* Unbless mode, opposite of Folder mode



### Mount your EFI partition to make changes

> diskutil list
/dev/disk0 (internal, physical):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:      GUID_partition_scheme                        *251.0 GB   disk0
   1:                        EFI EFI                     209.7 MB   disk0s1
   2:          Apple_CoreStorage Macintosh HD            190.7 GB   disk0s2
   3:                 Apple_Boot Recovery HD             650.1 MB   disk0s3
   4:                  Apple_HFS Linux HFS+ ESP          209.7 MB   disk0s4
   5:           Linux Filesystem                         524.3 MB   disk0s5
   6:                  Linux LVM                         58.7 GB    disk0s6
/dev/disk1 (internal, virtual):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:                  Apple_HFS Macintosh HD           +190.4 GB   disk1
                                 Logical Volume on disk0s2
                                 CBB5F777-9C7C-4EE2-ABC5-342891F61C21
                                 Unlocked Encrypted
