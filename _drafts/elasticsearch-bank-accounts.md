---
layout: post
title: Elasticsearch Bank Account
meta: elasticsearch account
categories:
- es6
- javascript
- web
- ruby
---

kibana
marvel

http://localhost:9200/_plugin/marvel/sense/index.html?


{% highlight bash %}
brew install elasticsearch
brew tap-pin jappievw/kibana
brew tap-pin jappievw/kibana
brew install kibana

cd /usr/local/Cellar/kibana/4.3.0

./bin/kibana plugin --install elastic/sense

elasticsearch
kibana
{% endhighlight bash %}


GET bank_transactions/_search
{
  "query": {
    "match_all": {}
  }
}

GET bank_transactions/_search?size=3513
{
  "query": {
    "match": {
      "payee": {
        "query": "Quevillon",
        "operator": "and"
      }
    }
  },
  "sort": { "date": { "order": "desc" }}
}

GET bank_transactions/_search?size=3513
{
  "query": {
    "match": {
      "payee": {
        "query": "Water",
        "operator": "and"
      }
    }
  },
  "sort": { "date": { "order": "desc" }}
}

GET bank_transactions/_search?size=3513
{
  "query":
{
  "filtered": {
    "query": {
      "match": {
        "payee": {
          "query": "L/B OF LAMBETH",
          "operator": "and"
        }
      }
    },
    "filter": {
      "bool": {
        "must": {
          "range": {
            "date": {
              "gt": "2015"
            }
          }
        }
      }
    }
  }
},"sort": { "date": { "order": "desc" }}
}

GET bank_transactions/_search?size=3513
{
  "query":
{
  "filtered": {
    "query": {
      "match": {
        "payee": {
          "query": "DIRECT DEBIT PAYMENT TO L/B OF LAMBETH",
          "operator": "and"
        }
      }
    },
    "filter": {
      "and": {
        "filters": [
          {"range": {
            "total": {
              "gt": -215,
              "lt": -100
            }
          }},
            {"range": {
            "date": {
              "gt": "2015-08-01"
            }
          }}
        ]
      }
    }
  }
},"sort": { "date": { "order": "desc" }},
"aggs": {
  "grand_total": {
    "sum": {
      "field": "total"
    }
  }
}
}

GET bank_transactions/_search?size=3513
{
  "query":
{
  "filtered": {
    "query": {
      "match": {
        "payee": {
          "query": "DIRECT DEBIT THAMES WATER",
          "operator": "and"
        }
      }
    },
    "filter": {
      "and": {
        "filters": [
            {"range": {
            "date": {
              "gt": "2015-08-01"
            }
          }}
        ]
      }
    }
  }
},"sort": { "date": { "order": "desc" }},
"aggs": {
  "grand_total": {
    "sum": {
      "field": "total"
    }
  }
}
}

GET bank_transactions/_search?size=3513
{
  "query":
{
  "filtered": {
    "query": {
      "match": {
        "payee": {
          "query": "VIRGIN",
          "operator": "and"
        }
      }
    },
    "filter": {
      "and": {
        "filters": [
            {"range": {
            "date": {
              "gt": "2015-08-01"
            }
          }}
        ]
      }
    }
  }
},"sort": { "date": { "order": "desc" }},
"aggs": {
  "grand_total": {
    "sum": {
      "field": "total"
    }
  }
}
}
