---
layout: post
title: Validate download checksum on Mac OSX
meta: security checksum macosx mac osx unix commandline
categories:
- security
- checksum
- mac
- osx
- bash
- unix
- commandline
---

If you want to verify the integrity of a software download such as Fedora using Mac OSX,
you can use the mac command line to do this.

https://getfedora.org/en/static/checksums/Fedora-Workstation-23-x86_64-CHECKSUM

brew install gpg

curl https://getfedora.org/static/fedora.gpg | gpg --import

curl https://getfedora.org/en/static/checksums/Fedora-Workstation-23-x86_64-CHECKSUM | gpg --verify-files
gpg --verify-files ~/Downloads/Fedora-Workstation-23-x86_64-CHECKSUM.txt

curl https://getfedora.org/en/static/checksums/Fedora-Workstation-23-x86_64-CHECKSUM | shasum -a 256 -t
shasum -a 256 -t ~/Downloads/Fedora-Workstation-23-x86_64-CHECKSUM.txt

shasum -a 256 -t ~/Downloads/Fedora-Workstation-netinst-x86_64-23.iso
