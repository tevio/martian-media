---
layout: post
title: Best static site generator
meta: static site generator
categories:
- static
- site
- generator
---

## jekyll

It seems that jekyll has won out when it comes to static site generators. There are countless plugins for doing endless kinds of things such as micro payments

Deploying your site to AWS S3, responsive images

s3_website lets you deploy straight to AWS

## middleman


## Hosting a static jekyll site for cheap
You can use heroku with a simple build pack
You can use AWS with s3_website

It seems that s3_website is faster for image loading

## images
go for 80 kb image size for the smallest images


## responsive background images for breakpoints
the best technique is the slice the images to the size of the maximum breakpoint they are almost the same as splash screen images

then some media queries to optimize page load times.

{% highlight css %}
@media only screen and (min-width : 0px) and (orientation:portrait) {
  #video-layout {
    background: url('/assets/images/backgrounds/320/IMG_0730-portrait.JPG') 50% 50% / cover no-repeat;
    background-attachment: fixed;
    background-position: center;
  }
}

@media only screen and (min-width : 320px) and (orientation:landscape) {
  #video-layout {
    background: url('/assets/images/backgrounds/320/IMG_0730-landscape.JPG') 50% 50% / cover no-repeat;
    background-attachment: fixed;
    background-position: center;
  }
}

@media only screen and (min-width : 320px) and (orientation:portrait) {
  #video-layout {
    background: url('/assets/images/backgrounds/640/IMG_0730-portrait.JPG') 50% 50% / cover no-repeat;
    background-attachment: fixed;
    background-position: center;
  }
}

@media only screen and (min-width : 320px) and (orientation:landscape) {
  #video-layout {
    background: url('/assets/images/backgrounds/640/IMG_0730-landscape.JPG') 50% 50% / cover no-repeat;
    background-attachment: fixed;
    background-position: center;
  }
}

/* Extra Small Devices, Phones */
@media only screen and (min-width : 480px) and (orientation:portrait) {
  #video-layout {
    background: url('/assets/images/backgrounds/640/IMG_0730-portrait.JPG') 50% 50% / cover no-repeat;
    background-attachment: fixed;
    background-position: center;
  }
}

@media only screen and (min-width : 480px) and (orientation:landscape) {
  #video-layout {
    background: url('/assets/images/backgrounds/640/IMG_0730-landscape.JPG') 50% 50% / cover no-repeat;
    background-attachment: fixed;
    background-position: center;
  }
}

/* Small Devices, Tablets */
@media only screen and (min-width : 768px) and (orientation:portrait) {
  #video-layout {
    background: url('/assets/images/backgrounds/1280/IMG_0730-portrait.JPG') 50% 50% / cover no-repeat;
    background-attachment: fixed;
    background-position: center;
  }
}

@media only screen and (min-width : 768px) and (orientation:landscape) {
  #video-layout {
    background: url('/assets/images/backgrounds/1280/IMG_0730-landscape.JPG') 50% 50% / cover no-repeat;
    background-attachment: fixed;
    background-position: center;
  }
}

/* Medium Devices, Desktops */
@media only screen and (min-width : 992px) and (orientation:portrait) {
  #video-layout {
    background: url('/assets/images/backgrounds/1280/IMG_0730.JPG') 50% 50% / cover no-repeat;
    background-attachment: fixed;
    background-position: center;
  }
}

@media only screen and (min-width : 992px) and (orientation:landscape) {
  #video-layout {
    background: url('/assets/images/backgrounds/1280/IMG_0730.JPG') 50% 50% / cover no-repeat;
    background-attachment: fixed;
    background-position: center;
  }
}

/* Large Devices, Wide Screens */
@media only screen and (min-width : 1200px) and (orientation:portrait) {
  #video-layout {
    background: url('/assets/images/backgrounds/1600/IMG_0730.JPG') 50% 50% / cover no-repeat;
    background-attachment: fixed;
    background-position: center;
  }
}

@media only screen and (min-width : 1200px) and (orientation:landscape) {
  #video-layout {
    background: url('/assets/images/backgrounds/1600/IMG_0730.JPG') 50% 50% / cover no-repeat;
    background-attachment: fixed;
    background-position: center;
  }
}

{% endhighlight css %}
