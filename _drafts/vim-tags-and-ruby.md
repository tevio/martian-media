---
layout: post
title: Vim, Tags & Full Stack Development
meta: vim 7.4 tags ruby javascript full-stack
categories:
- vim
- tags
- ruby
- js
- full-stack
---

Often as programmers we have itches we want to scratch for a long time. This is certainly one of mine.

Full stack Ruby developers often have to deal with many different syntaxes: Ruby, Javascript, SQL etc etc.

Tagging is a well known and effective way of mapping things in your codebase and making them accessible later and there are lots of resources out there for Vim users.

Ctags and Cscope are the most famous and venerable of these tools, sadly though, they don't work well with Ruby. Fear not though as there are several alternative tools out there that have been developed for tagging with Ruby, the ones I know of are [Ripper Tags](https://github.com/tmm1/ripper-tags) and [starscope](https://github.com/eapache/starscope).


### A solution
We can tackle the problem in two steps, one: generate one `tags` file and two: wrapping the commands in an easy to remember executable.

#### (RVM Caveat)
We want to include our app code and all the additional gem code, so we can traverse through the different code paths effectively, to do this we need to add the `GEM_HOME` envar (assuming you use RVM) to our starscope path, so the command looks like this:

{% highlight bash %}
starscope * `echo $GEM_HOME`/gems/* -e ctags
{% endhighlight bash %}

Note how you can pass a space seperated list of paths - this is the same with Ctags.

Set up your `~/.ctags` file like so:

{% highlight bash %}
  --exclude=**/*.haml
  --exclude=**/*.html.*
  --exclude=tmp*
  --exclude=coverage*
  --exclude=doc*
  --exclude=*.min.js
  --exclude=.git
  --exclude=log
{% endhighlight bash %}

{% highlight bash %}
ctags -R --append=yes **/*.js `echo $GEM_HOME`/gems/**/*.js --append=yes
{% endhighlight bash %}

This could all be wrapped up in a nice script:- `startags` and added to your `PATH` in which ever way you see fit.
{% highlight bash %}
  #!bin/sh
starscope * `echo $GEM_HOME`/gems/* -e ctags
ctags -R --append=yes **/*.js `echo $GEM_HOME`/gems/**/*.js --append=yes
{% endhighlight bash %}

Or you could configure your `tags` file, as per the Vim docs

{% highlight bash %}
starscope * `echo $GEM_HOME`/gems/* -e ctags,rtags
ctags -R -f jtags **/*.js `echo $GEM_HOME`/gems/**/*.js
{% endhighlight bash %}

then in vim

{% highlight bash %}
:set tags=jtags,rtags,tags
{% endhighlight bash %}

Or add this to your vimrc.

### The ripper-tags way
I'm going to focus on using the `ripper-tags` gem for the rest of this tutorial since it generates the tags file more quickly than starscope.

{% highlight bash %}
gem install ripper-tags
ripper-tags -R --tag-file=rtags2 * `echo $GEM_HOME`/gems/*
{% endhighlight bash %}

{% highlight bash %}
ripper-tags -R --tag-file=tags * `echo $GEM_HOME`/gems/*
ctags -R --append=yes **/*.js `echo $GEM_HOME`/gems/**/*.js --append=yes
mv tags tagsus
sort tagsus > tags
{% endhighlight bash %}


##### Footnotes

Great Vim plugin I was recently introduced to: Tagbar
