---
layout: post
title: Discover MAC addresses on you local network using OSX.
meta: network security MAC osx commandline
categories:
- osx
- network
- security
- commandline
---

## The Short
Run these two commands

{% highlight bash %}
brew install jq
arp -a | grep -v incomplete | cut -d ' ' -f 4 | xargs -I % curl http://www.macvendorlookup.com/api/v2/% | jq
{% endhighlight bash %}

## The Long
`arp` is a tool for checking addresses in the _Address Resolution Table_.

`arp -a` will give you a list of all the addresses, possibly something like:

{% highlight bash %}
? (25.255.255.255) at (incomplete) on ham0 ifscope [ethernet]
? (192.168.0.1) at 4c:60:de:1a:bc:67 on en0 ifscope [ethernet]
? (192.168.0.2) at (incomplete) on en0 ifscope [ethernet]
? (192.168.0.14) at 58:94:6b:48:3d:f0 on en0 ifscope [ethernet]
? (192.168.0.20) at 0:90:a9:d0:6b:f9 on en0 ifscope [ethernet]
? (192.168.0.21) at (incomplete) on en0 ifscope [ethernet]
? (192.168.0.22) at 90:b9:31:29:78:70 on en0 ifscope [ethernet]
? (192.168.0.24) at 3c:7:54:4a:e2:f2 on en0 ifscope [ethernet]
? (192.168.0.25) at (incomplete) on en0 ifscope [ethernet]
? (192.168.0.26) at 70:ec:e4:cd:ac:42 on en0 ifscope [ethernet]
? (192.168.0.255) at (incomplete) on en0 ifscope [ethernet]
{% endhighlight bash %}

Some of these, as you can see, will be __incomplete__, so we can filter them out using `grep` like so:-

`arp -a | grep -v incomplete`

OK! Now we're getting somewhere, we have a list with some MAC addresses that we can play with, but they're not very useful yet. We can fix that with the age old unix facility `cut`:-

`arp -a | grep -v incomplete | cut -d ' ' -f 4`

And we get...

{% highlight bash %}
4c:60:de:1a:bc:67
6c:ad:f8:9d:db:33
58:94:6b:48:3d:f0
0:90:a9:d0:6b:f9
3c:7:54:4a:e2:f2
{% endhighlight bash %}

... or similar.

Great now we have a nice clean list of MAC addresses that we can do interesting things with, like finding out what some of them *actually are*, using a nice little web service I found.

`arp -a | grep -v incomplete | cut -d ' ' -f 4 | xargs -I % curl http://www.macvendorlookup.com/api/v2/%`

Now we have some data about our MAC addresses, but alas, it is not pretty.

{% highlight bash %}
[{"startHex":"4C60DE000000","endHex":"4C60DEFFFFFF","startDec":"83978925113344","endDec":"83978941890559","company":"NETGEAR","addressL1":"350 East Plumeria Drive","addressL2":"","addressL3":"San Jose California 95134","country":"UNITED STATES","type":"MA-L"}][{"startHex":"6CADF8000000","endHex":"6CADF8FFFFFF","startDec":"119494445891584","endDec":"119494462668799","company":"Azurewave Technologies, Inc.","addressL1":"8F., No.120, Sec. 2, Kongdao 5 Rd., Hsinchu, Taiwan 30056","addressL2":"","addressL3":"Taipei Taiwan 231","country":"TAIWAN, PROVINCE OF CHINA","type":"MA-L"}][{"startHex":"58946B000000","endHex":"58946BFFFFFF","startDec":"97394473566208","endDec":"97394490343423","company":"Intel Corporate","addressL1":"Lot 8, Jalan Hi-Tech 2\/3","addressL2":"Kulim Hi-Tech Park","addressL3":"Kulim Kedah 09000","country":"MALAYSIA","type":"MA-L"}]%
{% endhighlight bash %}

Never fear, a cool utility is here!

`brew install jq`

This is the ( now defacto ) command line JSON parsing tool to Make All Things Pretty.

Now we can pipe the output of our `curl` into `jq`

`arp -a | grep -v incomplete | cut -d ' ' -f 4 | xargs -I % curl http://www.macvendorlookup.com/api/v2/% | jq`

Rinse and repeat.

##### Other Userful Network tools

* Nessus
* Nmap
* Zenmap
* p0f

##### File system tools

* chkrootkit
* rkhunter

Does scanning the WIFI scan the LAN also?
