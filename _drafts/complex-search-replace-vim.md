---
layout: post
title: Complex Search Replace with VIM 7.4
meta: vim 7.4 regex macro search replace
categories:
- vim
- macros
- edit
- search
- replace
---

## Output for Phraseapp
We can output all key values to CSV files, then:

http://stackoverflow.com/questions/21456682/ruby-csv-to-yaml

## Use cases
1. Plain text single line strings with no interpolation
2. Plain text single line strings with interpolation
3. Plain text multiline strings with no interpolation
4. PLain text multiline strings with interpolation

5. Text nested in HTML tags with no interpolation
6. Text nested in HTML tags with interpolation

## Global requirements
1. Create the namespace for the file
2. Convert erb block to i18n block in key value
2. Create the key value pair in the CSV file



Changing all chars to downcase `gu`

## Macro atoms
Replace visual selection with underscores.

`:'<,'>s/\%V /_/g`

Move to end of the beginning match

`/p>./e`
`//e`

## Step 1 - current file to namespace

To get the current file path in Vim

`i<Ctrl-R>%`

delete the file type segment `d3ge`

delete the views segment `^d4w`

change to key ( visual select and replace / with . )

`vE:'<,'>s/\//./g`

Then copy over to the other file

Then set mark `a` so we know where to look for the namespace

`:let @a='o%d3ge^d4wvE:s/\//./gv$ylphma'`

## Step 2 - Reformat the string as a key pair

`^v4eyPv^gu`

`:let @b='^v4eyPa v^guv4e:s/\%V /_/gkea:mb'`

But the line could have 1 or more words, how do we match without moving to a new line?

Manual visual selection, then:
`yPa v^gu`

Manual visual selection, then:
`:s/\%V /_/gkea:mb`

To get to the ceter of a line

`:echo col('.')`
`:let @z= col('.') / 2`

## Step 3

`:let @c="'avEy'bPa.q<80>kb"`

## Step 4

:let @d='^v4eyPa v^guv4e:s/\%V /_/gkea:'

## Step 5
:let @d='VylGph'

Step 1 - current file to namespace
Step 2 - convert content to key value
Step 3 - namespace the key in the key pair
Step 4 - copy the key pair to alt file
Step 5 - reformat the key as an i18n function call with key
Step 6 - wrap this in a macro


# Content in html tags with interpolation
Copy the first line
yyp

Change the left delimiter
:s/<%=/&#123;%/
:s/%>/}/
/.}
vi}
:s/\%V@//
:s/\%V\./_/
/>./e

1. Convert the interpolation
yyp:s/<%=/&#123;%/ :s/%>/}/ /.} vi} :s/\%V@// :s/\%V\./_/ h

2. Build the key
/>./evt{*ngnyPgNguvnb:s/\%V /_/g
Match first tag
/<*>./e
vgn

:s/\%V%>/}/

