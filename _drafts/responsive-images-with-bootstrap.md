---
layout: post
title: Responsive images with bootstrap
meta: Responsive images with bootstrap
categories:
- responsive design
- images
- bootstrap
---

When designing a page with a responsive layout, many people turn to the the most popular project on github - bootstrap.

With the new fandangled HTML5 tools for handling images this gitves us a lot more options

## Background images
Perhaps you're using a full screen image for a background.

Media queries offer orientation selectors which means you can optimise background image load times by cropping images appropriately and then loading the best image using media queries

## Card images

## Banner images
