---
layout: post
title: Making animated GIF tutorials with OSX
meta: animated gif tutorial
categories:
- gif
- animated
- tutorial
- html
---

## Generating the animations

You can use Quicktime

Then convert to images with ffmpeg

ffmpeg -i generate-public-private-keypair.mov -r 10 -vcodec png out-static-%3d.png

NOTE - the `%3d` is very important to enable you to not think about ordering the images when using convert.

Now pick the ones you want

And then convert to gif

convert -delay 50 -loop 0 \*.png animation.gif

convert -delay 50 'out-static-%03d.png[0-1000]' animation.gif

Be careful with the numeric matching.
